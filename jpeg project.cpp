// djvu project.cpp : Defines the entry pofloat for the console application.
//

#include "stdafx.h"
#define cimg_use_jpeg
#include "CImg.h"
#include "arithmetical_operations.cpp"
#include <iostream>
#include <string>
#include <list>
using namespace cimg_library;
using namespace std;
int main()
{
	CImg<float> *image1 = new CImg<float>();
	CImg<float> *image2 = new CImg<float>();
	CImgDisplay *image1Display = new CImgDisplay();
	CImgDisplay *image2Display = new CImgDisplay();
	string input;
	CImgList<float> additionalImages;
	list<CImgDisplay> additionalDisplays;
	while (true) {
		cout << "Press to:" << endl
			<< "1.Load image" << endl
			<< "2.Load second image" << endl
			<< "3. Add images" << endl
			<< "4. Add value to image" << endl
			<< "5. Multiply images" << endl
			<< "6. Multiply image by value" << endl
			<< "7. Power image to value" << endl
			<< "c. Close program" << endl;
		getline(cin, input);
		if (input == "c") {
			delete image1;
			delete image2;
			delete image1Display;
			delete image2Display;
			cin.clear();
			input.clear();
			input.erase();
			break;
		}
		if (input == "1") {
			cout << "Image name" << endl;
			char table[256] = "a"; //awful hack, but works
			char* name;
			name = table;
			cin.getline(name, 256);
			delete image1;
			image1 = new CImg<float>(name);
			delete image1Display;
			image1Display = new CImgDisplay(*image1);
		}
		if (input == "2") {
			cout << "Image name" << endl;
			char table2[256] = "a"; //awful hack, but works
			char* name2;
			name2 = table2;
			cin.getline(name2, 256);
			/*
			//strcpy((char*)name2, nameInString.);
			cin.clear();
			cin.clear();
			cin >> name2;
			cout << endl;
			*/
			delete image2;
			image2 = new CImg<float>(name2);
			delete image2Display;
			image2Display = new CImgDisplay(*image2);
		}
		if (input == "3") {
			CImg<float> addedImages = *image1 + *image2;
			addedImages.cut(0, 255); //Necessary? Certaincly gives different output than uncut version.
			CImgDisplay addedImagesDisplay(addedImages);
			additionalImages.insert(addedImages);
			additionalDisplays.push_back(addedImagesDisplay);

		}
		if (input == "4"){
			string contextInput;
			cout << "Input value:" << endl;
			getline(cin, contextInput);
			float value = stof(contextInput);
			cout << "To which image the value should be added?" << endl;
			getline(cin, contextInput);
			CImg<float> imageMultipliedByValue;
			if (contextInput == "1") {
				imageMultipliedByValue = *image1 + value;
			}
			if (contextInput == "2"){
				imageMultipliedByValue = *image2 + value;
			}
			imageMultipliedByValue.cut(0, 255);
			additionalImages.insert(imageMultipliedByValue);
			CImgDisplay imageMultipliedByValueDisplay(imageMultipliedByValue);
			additionalDisplays.push_back(imageMultipliedByValueDisplay);
		}
		if (input == "5") {
			CImg<float> multipliedImages(image1->mul(*image2)); //Pointers...
			multipliedImages.cut(0, 255); //Necessary?
			additionalImages.insert(multipliedImages);
			CImgDisplay multipliedImagesDisplay(multipliedImages);
			additionalDisplays.push_back(multipliedImagesDisplay);
		}
		if (input == "6") {
			string contextInput;
			cout << "Input value:" << endl;
			getline(cin, contextInput);
			float value = stof(contextInput);
			cout << "Which image should be multiplied by this value?" << endl;
			getline(cin, contextInput);
			CImg<float> imageMultipliedByValue;
			if (contextInput == "1") {
				imageMultipliedByValue = *image1 * value;
			}
			if (contextInput == "2") {
				imageMultipliedByValue = *image2 * value;
			}
			imageMultipliedByValue.cut(0, 255);
			additionalImages.insert(imageMultipliedByValue);
			CImgDisplay imageMultipliedByValueDisplay(imageMultipliedByValue);
			additionalDisplays.push_back(imageMultipliedByValueDisplay);
		}
		/*
		switch (input) {
		case 1:
			cout << "Image name" << endl;
			unsigned char name;
			cin >> name;
			image1 = new CImg<float>(name);
			image1Display = new CImgDisplay(*image1);
			break;
		case 2:
			cout << "Image name" << endl;
			unsigned char name2;
			cin >> name2;
			image2 = new CImg<float>(name2);
			image2Display = new CImgDisplay(*image2);
			break;
		case 8:
			return 0;
		}
		*/
	}
	return 0;
}
	/*
	CImg<float> image("input.jpeg");
	CImg<float> image2("input2.jpeg");
	CImgDisplay mainDisp(image);
	CImgDisplay disp2(image2);
	float a = *image.data(0, 0, 0, 0);
	float b = *image.data(0, 0, 0, 1);
	float c = *image.data(0, 0, 0, 2);
	//float value;
	cout << "Input value image shall be multiplied by" << endl;
	//cin >> value;
	//CImg<float> newImage(multiplyImageByValue(image, value));
	CImg<float> multiplicationOfImages(image.mul(image2)); //mnożenie obrazów
	CImg<float> additionOfImages = image + image2; //dodawanie obrazów
	//float d = *newImage.data(0, 0, 0, 0);
	//float e = *newImage.data(0, 0, 0, 1);
	//float f = *newImage.data(0, 0, 0, 2);
	float d = *multiplicationOfImages.data(0, 0, 0, 0);
	float e = *multiplicationOfImages.data(0, 0, 0, 1);
	float f = *multiplicationOfImages.data(0, 0, 0, 2);
	float g = *additionOfImages.data(0, 0, 0, 0);
	float h = *additionOfImages.data(0, 0, 0, 1);
	float i = *additionOfImages.data(0, 0, 0, 2);
	//CImgDisplay disp2(newImage);
	
	CImgDisplay disp3(multiplicationOfImages);
	CImgDisplay disp4(additionOfImages);
	while (!mainDisp.is_closed() && !disp3.is_closed() && !disp4.is_closed()) {
		mainDisp.wait();
		//disp2.wait();
		disp3.wait();

	}
	*/

