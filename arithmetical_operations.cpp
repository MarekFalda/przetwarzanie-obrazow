#include "stdafx.h"
#include "CImg.h"
#define cimg_use_jpeg
using namespace cimg_library;
#ifndef arithmetical_operations_hpp
#define arithmetical_operations_hpp
#define cimg_use_jpeg

template <class T>
CImg<T> multiplyImageByValue(CImg<T>& inputImage, double value) {
	CImg<T> returnImage(inputImage);
	returnImage *= value;
	return returnImage;
}

#endif // !
